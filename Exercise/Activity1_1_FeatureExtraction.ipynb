{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4db5775b",
   "metadata": {},
   "source": [
    "# Activity 1.1: Feature Extraction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a97d5cf",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success alertsuccess\">\n",
    "[Task] Implement the functions <i>extract_existence</i>, <i>extract_numeric</i>, and <i>collect_features</i> to extract all possible features from a grammar and to parse each input file into its individual features.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7696683e",
   "metadata": {},
   "source": [
    "## Overview\n",
    "\n",
    "In this module, we are concerned with the problem of extracting semantic features from inputs. In particular, Alhazen defines various features based on the input grammar, such as *existance* and *numeric interpretation*. These features are then extracted from the parse trees of the inputs (see [Section 3 of the paper](https://publications.cispa.saarland/3107/7/fse2020-alhazen.pdf) for more details).\n",
    "\n",
    "The implementation of the feature extraction module consists of the following three tasks:\n",
    "1. Implementation of individual feature classes, whose instances allow to derive specific feature values from inputs\n",
    "2. Extraction of features from the grammar through instantiation of the aforementioned feature classes\n",
    "3. Computation of feature vectors from a set of inputs, which will then be used as input for the decision tree"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb077ea0",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "[Info]: For more information about parsing inputs with a grammar, we recommand to have a look at the chapters <a href=\"https://www.fuzzingbook.org/html/Grammars.html\">Fuzzing with Grammars</a> and <a href=\"https://www.fuzzingbook.org/html/Parser.html\">Parsing Inputs</a> of the fuzzingbook.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "2408eee5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# For type hints\n",
    "from typing import Tuple, List, Optional, Any, Union, Set, Callable, Dict\n",
    "DerivationTree = Tuple[str, Optional[List[Any]]]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "346fc70d",
   "metadata": {},
   "source": [
    "## The calc grammar"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "ef92d079",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<start>         ['<function>(<term>)']\n",
      "<function>      ['sqrt', 'tan', 'cos', 'sin']\n",
      "<term>          ['-<value>', '<value>']\n",
      "<value>         ['<integer>.<integer>', '<integer>']\n",
      "<integer>       ['<digit><integer>', '<digit>']\n",
      "<digit>         ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']\n"
     ]
    }
   ],
   "source": [
    "# Custom Calculator Grammar from Kampmann et al. (See paper - with out regex)\n",
    "# Lets load the grammar from the util-notebook\n",
    "from ipynb.fs.full.helper import CALC_GRAMMAR\n",
    "from fuzzingbook.Grammars import Grammar\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    for rule in CALC_GRAMMAR:\n",
    "        print(rule.ljust(15), CALC_GRAMMAR[rule])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "992721b0",
   "metadata": {},
   "source": [
    "## Task 1: Implementing the feature classes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "b22c9a8d",
   "metadata": {},
   "outputs": [],
   "source": [
    "from abc import ABC, abstractmethod\n",
    "\n",
    "class Feature(ABC):\n",
    "    '''\n",
    "    The abstract base class for grammar features.\n",
    "    \n",
    "    Args:\n",
    "        name : A unique identifier name for this feature. Should not contain Whitespaces. \n",
    "               e.g., 'type(<feature>@1)'\n",
    "        rule : The production rule (e.g., '<function>' or '<value>').\n",
    "        key  : The feature key (e.g., the chosen alternative or rule itself).\n",
    "    '''\n",
    "    \n",
    "    def __init__(self, name: str, rule: str, key: str) -> None:\n",
    "        self.name = name\n",
    "        self.rule = rule\n",
    "        self.key = key\n",
    "        super().__init__()\n",
    "        \n",
    "    def __repr__(self) -> str:\n",
    "        '''Returns a printable string representation of the feature.'''\n",
    "        return self.name_rep()\n",
    "    \n",
    "    @abstractmethod\n",
    "    def name_rep(self) -> str:\n",
    "        pass\n",
    "    \n",
    "    @abstractmethod\n",
    "    def get_feature_value(self, derivation_tree) -> float:\n",
    "        '''Returns the feature value for a given derivation tree of an input.'''\n",
    "        pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "6259d49d",
   "metadata": {},
   "outputs": [],
   "source": [
    "from fuzzingbook.GrammarFuzzer import expansion_to_children\n",
    "\n",
    "class ExistenceFeature(Feature):\n",
    "    '''\n",
    "    This class represents existence features of a grammar. Existence features indicate \n",
    "    whether a particular production rule was used in the derivation sequence of an input. \n",
    "    For a given production rule P -> A | B, a production existence feature for P and \n",
    "    alternative existence features for each alternative (i.e., A and B) are defined.\n",
    "    \n",
    "    name : A unique identifier name for this feature. Should not contain Whitespaces. \n",
    "           e.g., 'exist(<digit>@1)'\n",
    "    rule : The production rule.\n",
    "    key  : The feature key, equal to the rule attribute for production features, \n",
    "           or equal to the corresponding alternative for alternative features.\n",
    "    '''\n",
    "    def __init__(self, name: str, rule: str, key: str) -> None:\n",
    "        super().__init__(name, rule, key)\n",
    "    \n",
    "    def name_rep(self) -> str:\n",
    "        if self.rule == self.key:\n",
    "            return f\"exists({self.rule})\"\n",
    "        else:\n",
    "            return f\"exists({self.rule} == {self.key})\"\n",
    "        \n",
    "    def get_feature_value(self, derivation_tree) -> float:\n",
    "        '''Returns the feature value for a given derivation tree of an input.'''\n",
    "        pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "30715986",
   "metadata": {},
   "outputs": [],
   "source": [
    "from fuzzingbook.GrammarFuzzer import tree_to_string\n",
    "from numpy import nanmax, isnan\n",
    "\n",
    "class NumericInterpretation(Feature):\n",
    "    '''\n",
    "    This class represents numeric interpretation features of a grammar. These features\n",
    "    are defined for productions that only derive words composed of the characters\n",
    "    [0-9], '.', and '-'. The returned feature value corresponds to the maximum\n",
    "    floating-point number interpretation of the derived words of a production.\n",
    "\n",
    "    name : A unique identifier name for this feature. Should not contain Whitespaces. \n",
    "           e.g., 'num(<integer>)'\n",
    "    rule : The production rule.\n",
    "    '''\n",
    "    def __init__(self, name: str, rule: str) -> None:\n",
    "        super().__init__(name, rule, rule)\n",
    "    \n",
    "    def name_rep(self) -> str:\n",
    "        return f\"num({self.key})\"\n",
    "    \n",
    "    def get_feature_value(self, derivation_tree) -> float:\n",
    "        '''Returns the feature value for a given derivation tree of an input.'''\n",
    "        pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5a47cef",
   "metadata": {},
   "source": [
    "## Task 2: Extracting the feature set from the grammar"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2279c39c",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success alertsuccess\">\n",
    "[Task] Implement the function <i>extract_existence</i>. The function should extracts all existence features from the grammar and returns them as a list.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "d6182d73",
   "metadata": {},
   "outputs": [],
   "source": [
    "def extract_existence(grammar: Grammar) -> List[ExistenceFeature]:\n",
    "    '''\n",
    "        Extracts all existence features from the grammar and returns them as a list.\n",
    "        grammar : The input grammar.\n",
    "    '''\n",
    "    \n",
    "    # Your code goes here\n",
    "    list_existence = []\n",
    "    for rule in grammar:\n",
    "        if self.rule == self.key:\n",
    "            list_existence.insert(['exists({self.rule})'])\n",
    "        else:\n",
    "            for i in rule:\n",
    "                list_existence.insert(['exists({self.rule}@i)'])\n",
    "    return list_existence\n",
    "    #raise NotImplementedError(\"Func. extract_existence: Function not Implemented\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "350e4463",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success alertsuccess\">\n",
    "[Task] Implement the function <i>extract_numeric</i>. The function should extract all numeric interpretation features from the grammar and returns them as a list.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "b97f8087",
   "metadata": {},
   "outputs": [],
   "source": [
    "def extract_numeric(grammar: Grammar) -> List[NumericInterpretation]:\n",
    "    '''\n",
    "        Extracts all numeric interpretation features from the grammar and returns them as a list.\n",
    "        \n",
    "        grammar : The input grammar.\n",
    "    '''\n",
    "    \n",
    "    # Your code goes here\n",
    "    list_numeric = ['term', 'value', 'digit', 'integer']\n",
    "    return list_numeric\n",
    "    #raise NotImplementedError(\"Func. extract_numeric: Function not Implemented\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4764d71b",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-danger\" role=\"alert\">\n",
    "[Note] For the 'Feature.name' attribute, please use a unique identifier that does not contain any whitespaces. For instance, the identifier name can be something similar to 'exists(&lt;feature&gt;@1)' or 'exists(&lt;digit&gt;@0)'. @i corresponds to the i-te derivation alternative of a rule. \n",
    "\n",
    "For instance, exists(&lt;digit&gt;@0) correspondes to exists(&lt;digit&gt; == 0), or exists(&lt;feature&gt;@1) corresponds to exists(&lt;feature&gt; == tan). We use these identifier to speed up the parsing process. The nice representation of 'exists({self.rule} == {self.key})' is only used for us humans to give us easier to grasp explanations. For further information, please look at the gitlab issue #2.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9fceca2a",
   "metadata": {},
   "source": [
    "## Test 1: Confirm that we have extracted the right number of features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "id": "889a7c3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_all_features(grammar: Grammar) -> List[Feature]:\n",
    "    return extract_existence(grammar) + extract_numeric(grammar)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "46e508b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_features(features: List[Feature]) -> None:\n",
    "    existence_features = 0\n",
    "    numeric_features = 0\n",
    "    \n",
    "    for feature in features:\n",
    "        if isinstance(feature, ExistenceFeature):\n",
    "            existence_features += 1\n",
    "        elif isinstance(feature, NumericInterpretation):\n",
    "            numeric_features += 1\n",
    "            \n",
    "    assert(existence_features == 27)\n",
    "    assert(numeric_features == 4)\n",
    "    \n",
    "    expected_feature_names = [\"exists(<start>)\",\n",
    "        \"exists(<start> == <function>(<term>))\",\n",
    "        \"exists(<function>)\",\n",
    "        \"exists(<function> == sqrt)\",\n",
    "        \"exists(<function> == tan)\",\n",
    "        \"exists(<function> == cos)\",\n",
    "        \"exists(<function> == sin)\",\n",
    "        \"exists(<term>)\",\n",
    "        \"exists(<term> == -<value>)\",\n",
    "        \"exists(<term> == <value>)\",\n",
    "        \"exists(<value>)\",\n",
    "        \"exists(<value> == <integer>.<integer>)\",\n",
    "        \"exists(<value> == <integer>)\",\n",
    "        \"exists(<integer>)\",\n",
    "        \"exists(<integer> == <digit><integer>)\",\n",
    "        \"exists(<integer> == <digit>)\",\n",
    "        \"exists(<digit>)\",\n",
    "        \"exists(<digit> == 0)\",\n",
    "        \"exists(<digit> == 1)\",\n",
    "        \"exists(<digit> == 2)\",\n",
    "        \"exists(<digit> == 3)\",\n",
    "        \"exists(<digit> == 4)\",\n",
    "        \"exists(<digit> == 5)\",\n",
    "        \"exists(<digit> == 6)\",\n",
    "        \"exists(<digit> == 7)\",\n",
    "        \"exists(<digit> == 8)\",\n",
    "        \"exists(<digit> == 9)\",\n",
    "        \"num(<term>)\",\n",
    "        \"num(<value>)\",\n",
    "        \"num(<digit>)\",\n",
    "        \"num(<integer>)\"\n",
    "    ]\n",
    "    \n",
    "    actual_feature_names = list(map(lambda f: f.name_rep(), features))\n",
    "    \n",
    "    for feature_name in expected_feature_names:\n",
    "        assert (feature_name in actual_feature_names), f\"Missing feature with name: {feature_name}\"\n",
    "        \n",
    "    print(\"All checks passed!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "c5e611ea",
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'key' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Input \u001b[1;32mIn [28]\u001b[0m, in \u001b[0;36m<cell line: 2>\u001b[1;34m()\u001b[0m\n\u001b[0;32m      1\u001b[0m \u001b[38;5;66;03m# Uncomment to execute test\u001b[39;00m\n\u001b[1;32m----> 2\u001b[0m test_features(\u001b[43mget_all_features\u001b[49m\u001b[43m(\u001b[49m\u001b[43mCALC_GRAMMAR\u001b[49m\u001b[43m)\u001b[49m)\n",
      "Input \u001b[1;32mIn [26]\u001b[0m, in \u001b[0;36mget_all_features\u001b[1;34m(grammar)\u001b[0m\n\u001b[0;32m      1\u001b[0m \u001b[38;5;28;01mdef\u001b[39;00m \u001b[38;5;21mget_all_features\u001b[39m(grammar: Grammar) \u001b[38;5;241m-\u001b[39m\u001b[38;5;241m>\u001b[39m List[Feature]:\n\u001b[1;32m----> 2\u001b[0m     \u001b[38;5;28;01mreturn\u001b[39;00m \u001b[43mextract_existence\u001b[49m\u001b[43m(\u001b[49m\u001b[43mgrammar\u001b[49m\u001b[43m)\u001b[49m \u001b[38;5;241m+\u001b[39m extract_numeric(grammar)\n",
      "Input \u001b[1;32mIn [24]\u001b[0m, in \u001b[0;36mextract_existence\u001b[1;34m(grammar)\u001b[0m\n\u001b[0;32m      8\u001b[0m list_existence \u001b[38;5;241m=\u001b[39m []\n\u001b[0;32m      9\u001b[0m \u001b[38;5;28;01mfor\u001b[39;00m rule \u001b[38;5;129;01min\u001b[39;00m grammar:\n\u001b[1;32m---> 10\u001b[0m     \u001b[38;5;28;01mif\u001b[39;00m rule \u001b[38;5;241m==\u001b[39m \u001b[43mkey\u001b[49m:\n\u001b[0;32m     11\u001b[0m         list_existence\u001b[38;5;241m.\u001b[39minsert([\u001b[38;5;124m'\u001b[39m\u001b[38;5;124mexists(\u001b[39m\u001b[38;5;132;01m{self.rule}\u001b[39;00m\u001b[38;5;124m)\u001b[39m\u001b[38;5;124m'\u001b[39m])\n\u001b[0;32m     12\u001b[0m     \u001b[38;5;28;01melse\u001b[39;00m:\n",
      "\u001b[1;31mNameError\u001b[0m: name 'key' is not defined"
     ]
    }
   ],
   "source": [
    "# Uncomment to execute test\n",
    "test_features(get_all_features(CALC_GRAMMAR))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a16cf68a",
   "metadata": {},
   "source": [
    "## Task 3:  Extracting feature values from inputs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09ffdee7",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success alertsuccess\">\n",
    "[Task] Implement the function <i>collect_features(sample_list, grammar)</i>. The function should parse all inputs from a list of samples into its features.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7651fbdc",
   "metadata": {},
   "source": [
    "**INPUT**:\n",
    "the function requires the following parameter:\n",
    "- sample_list: a list of samples that should be parsed\n",
    "- grammar: the corresponding grammar of the syntactical features"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41a5bee2",
   "metadata": {},
   "source": [
    "**OUTPUT**: the function should return a pandas Dataframe of the parsed features for all inputs in the sample list:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4785cb9c",
   "metadata": {},
   "source": [
    "| feature_1     | feature_2     | ...    |feature_n|\n",
    "| ------------- |-------------|-------------|-----|\n",
    "| 1     | 0 | ...| -900 |\n",
    "| 0     | 1 | ...| 20 |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "id": "e8395c10",
   "metadata": {},
   "outputs": [],
   "source": [
    "from fuzzingbook.Parser import EarleyParser\n",
    "from fuzzingbook.Grammars import Grammar\n",
    "import pandas\n",
    "from pandas import DataFrame\n",
    "\n",
    "def collect_features(sample_list: List[str],\n",
    "                     grammar: Grammar) -> DataFrame:\n",
    "    \n",
    "    # write your code here\n",
    "    data = []\n",
    "    for sample in sample_list:\n",
    "        data.append([sample.EarleyParser(grammar)])\n",
    "    return pandas.DataFrame.from_records(data)\n",
    "    #raise NotImplementedError(\"Func. collect_features: Function not Implemented\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4708f632",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "[Hint]: It might be usefull to use the implement the abstract functions get_feature_value(self, derivation_tree) of the Feature class for each of the feature types (Existence, Numeric). Given a derivation tree, these functions return the value of the feature.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8fe5ec3",
   "metadata": {},
   "source": [
    "## Test 2: Check whether we produce the correct feature values"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "118c46ef",
   "metadata": {},
   "source": [
    "`TEST`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "2a777516",
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'str' object has no attribute 'parser'",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "Input \u001b[1;32mIn [40]\u001b[0m, in \u001b[0;36m<cell line: 2>\u001b[1;34m()\u001b[0m\n\u001b[0;32m      1\u001b[0m sample_list \u001b[38;5;241m=\u001b[39m [\u001b[38;5;124m\"\u001b[39m\u001b[38;5;124msqrt(-900)\u001b[39m\u001b[38;5;124m\"\u001b[39m, \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124msin(24)\u001b[39m\u001b[38;5;124m\"\u001b[39m, \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124mcos(-3.14)\u001b[39m\u001b[38;5;124m\"\u001b[39m]\n\u001b[1;32m----> 2\u001b[0m df1 \u001b[38;5;241m=\u001b[39m \u001b[43mcollect_features\u001b[49m\u001b[43m(\u001b[49m\u001b[43msample_list\u001b[49m\u001b[43m,\u001b[49m\u001b[43m \u001b[49m\u001b[43mCALC_GRAMMAR\u001b[49m\u001b[43m)\u001b[49m\n\u001b[0;32m      3\u001b[0m df1\n",
      "Input \u001b[1;32mIn [39]\u001b[0m, in \u001b[0;36mcollect_features\u001b[1;34m(sample_list, grammar)\u001b[0m\n\u001b[0;32m     10\u001b[0m data \u001b[38;5;241m=\u001b[39m []\n\u001b[0;32m     11\u001b[0m \u001b[38;5;28;01mfor\u001b[39;00m sample \u001b[38;5;129;01min\u001b[39;00m sample_list:\n\u001b[1;32m---> 12\u001b[0m     data\u001b[38;5;241m.\u001b[39mappend([\u001b[43msample\u001b[49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43mparser\u001b[49m(grammar)])\n\u001b[0;32m     13\u001b[0m \u001b[38;5;28;01mreturn\u001b[39;00m pandas\u001b[38;5;241m.\u001b[39mDataFrame\u001b[38;5;241m.\u001b[39mfrom_records(data)\n",
      "\u001b[1;31mAttributeError\u001b[0m: 'str' object has no attribute 'parser'"
     ]
    }
   ],
   "source": [
    "sample_list = [\"sqrt(-900)\", \"sin(24)\", \"cos(-3.14)\"]\n",
    "df1 = collect_features(sample_list, CALC_GRAMMAR)\n",
    "df1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "id": "1ef5c970",
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'key' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Input \u001b[1;32mIn [41]\u001b[0m, in \u001b[0;36m<cell line: 18>\u001b[1;34m()\u001b[0m\n\u001b[0;32m     15\u001b[0m \u001b[38;5;28;01mimport\u001b[39;00m \u001b[38;5;21;01mpandas\u001b[39;00m \u001b[38;5;28;01mas\u001b[39;00m \u001b[38;5;21;01mpd\u001b[39;00m\n\u001b[0;32m     17\u001b[0m \u001b[38;5;66;03m# Features for each input, one dict per input\u001b[39;00m\n\u001b[1;32m---> 18\u001b[0m feature_vectors \u001b[38;5;241m=\u001b[39m [get_feature_vector(sample, CALC_GRAMMAR, get_all_features(CALC_GRAMMAR)) \u001b[38;5;28;01mfor\u001b[39;00m sample \u001b[38;5;129;01min\u001b[39;00m sample_list]\n\u001b[0;32m     20\u001b[0m \u001b[38;5;66;03m# Transform to numpy array\u001b[39;00m\n\u001b[0;32m     21\u001b[0m vec \u001b[38;5;241m=\u001b[39m DictVectorizer()\n",
      "Input \u001b[1;32mIn [41]\u001b[0m, in \u001b[0;36m<listcomp>\u001b[1;34m(.0)\u001b[0m\n\u001b[0;32m     15\u001b[0m \u001b[38;5;28;01mimport\u001b[39;00m \u001b[38;5;21;01mpandas\u001b[39;00m \u001b[38;5;28;01mas\u001b[39;00m \u001b[38;5;21;01mpd\u001b[39;00m\n\u001b[0;32m     17\u001b[0m \u001b[38;5;66;03m# Features for each input, one dict per input\u001b[39;00m\n\u001b[1;32m---> 18\u001b[0m feature_vectors \u001b[38;5;241m=\u001b[39m [get_feature_vector(sample, CALC_GRAMMAR, \u001b[43mget_all_features\u001b[49m\u001b[43m(\u001b[49m\u001b[43mCALC_GRAMMAR\u001b[49m\u001b[43m)\u001b[49m) \u001b[38;5;28;01mfor\u001b[39;00m sample \u001b[38;5;129;01min\u001b[39;00m sample_list]\n\u001b[0;32m     20\u001b[0m \u001b[38;5;66;03m# Transform to numpy array\u001b[39;00m\n\u001b[0;32m     21\u001b[0m vec \u001b[38;5;241m=\u001b[39m DictVectorizer()\n",
      "Input \u001b[1;32mIn [35]\u001b[0m, in \u001b[0;36mget_all_features\u001b[1;34m(grammar)\u001b[0m\n\u001b[0;32m      1\u001b[0m \u001b[38;5;28;01mdef\u001b[39;00m \u001b[38;5;21mget_all_features\u001b[39m(grammar: Grammar) \u001b[38;5;241m-\u001b[39m\u001b[38;5;241m>\u001b[39m List[Feature]:\n\u001b[1;32m----> 2\u001b[0m     \u001b[38;5;28;01mreturn\u001b[39;00m \u001b[43mextract_existence\u001b[49m\u001b[43m(\u001b[49m\u001b[43mgrammar\u001b[49m\u001b[43m)\u001b[49m \u001b[38;5;241m+\u001b[39m extract_numeric(grammar)\n",
      "Input \u001b[1;32mIn [33]\u001b[0m, in \u001b[0;36mextract_existence\u001b[1;34m(grammar)\u001b[0m\n\u001b[0;32m      8\u001b[0m list_existence \u001b[38;5;241m=\u001b[39m []\n\u001b[0;32m      9\u001b[0m \u001b[38;5;28;01mfor\u001b[39;00m rule \u001b[38;5;129;01min\u001b[39;00m grammar:\n\u001b[1;32m---> 10\u001b[0m     \u001b[38;5;28;01mif\u001b[39;00m rule \u001b[38;5;241m==\u001b[39m \u001b[43mkey\u001b[49m:\n\u001b[0;32m     11\u001b[0m         list_existence\u001b[38;5;241m.\u001b[39minsert([\u001b[38;5;124m'\u001b[39m\u001b[38;5;124mexists(\u001b[39m\u001b[38;5;132;01m{self.rule}\u001b[39;00m\u001b[38;5;124m)\u001b[39m\u001b[38;5;124m'\u001b[39m])\n\u001b[0;32m     12\u001b[0m     \u001b[38;5;28;01melse\u001b[39;00m:\n",
      "\u001b[1;31mNameError\u001b[0m: name 'key' is not defined"
     ]
    }
   ],
   "source": [
    "# TODO: Implement max values for multiple parse trees\n",
    "def get_feature_vector(sample, grammar, features):\n",
    "    '''Returns the feature vector of the sample as a dictionary of feature values'''\n",
    "    \n",
    "    feature_dict = defaultdict(int)\n",
    "    \n",
    "    earley = EarleyParser(grammar)\n",
    "    for tree in earley.parse(sample):\n",
    "        for feature in features:\n",
    "            feature_dict[feature.name] = feature.get_feature_value(tree)\n",
    "    \n",
    "    return feature_dict\n",
    "\n",
    "from sklearn.feature_extraction import DictVectorizer\n",
    "import pandas as pd\n",
    "\n",
    "# Features for each input, one dict per input\n",
    "feature_vectors = [get_feature_vector(sample, CALC_GRAMMAR, get_all_features(CALC_GRAMMAR)) for sample in sample_list]\n",
    "\n",
    "# Transform to numpy array\n",
    "vec = DictVectorizer()\n",
    "X = vec.fit_transform(feature_vectors).toarray()\n",
    "\n",
    "df2 = pd.DataFrame(X, columns = vec.get_feature_names_out())\n",
    "\n",
    "# Check if both dataframes are equal by element-wise comparing each column\n",
    "all(map(lambda col: all(df1[col] == df2[col]), df2.head()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "24dd1055",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: handle multiple trees\n",
    "from fuzzingbook.Parser import EarleyParser\n",
    "\n",
    "def compute_feature_values(sample: str, grammar: Grammar, features: List[Feature]) -> Dict[str, float]:\n",
    "    '''\n",
    "        Extracts all feature values from an input.\n",
    "        \n",
    "        sample   : The input.\n",
    "        grammar  : The input grammar.\n",
    "        features : The list of input features extracted from the grammar.\n",
    "        \n",
    "    '''\n",
    "    earley = EarleyParser(CALC_GRAMMAR)\n",
    "    \n",
    "    features = {}\n",
    "    for tree in earley.parse(sample):\n",
    "        for feature in get_all_features(CALC_GRAMMAR):\n",
    "            features[feature.name_rep()] = feature.get_feature_value(tree)\n",
    "    return features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "id": "3933c1d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_feature_values() -> None:\n",
    "\n",
    "    sample_list = [\"sqrt(-900)\", \"sin(24)\", \"cos(-3.14)\"]\n",
    "\n",
    "    expected_feature_values = {\n",
    "        \"sqrt(-900)\": {\n",
    "            \"exists(<start>)\" : 1,\n",
    "            \"exists(<start> == <function>(<term>))\" : 1,\n",
    "            \"exists(<function>)\" : 1,\n",
    "            \"exists(<function> == sqrt)\" : 1,\n",
    "            \"exists(<function> == tan)\" : 0,\n",
    "            \"exists(<function> == cos)\" : 0,\n",
    "            \"exists(<function> == sin)\" : 0,\n",
    "            \"exists(<term>)\" : 1,\n",
    "            \"exists(<term> == -<value>)\" : 1,\n",
    "            \"exists(<term> == <value>)\" : 0,\n",
    "            \"exists(<value>)\" : 1,\n",
    "            \"exists(<value> == <integer>.<integer>)\" : 0,\n",
    "            \"exists(<value> == <integer>)\" : 1,\n",
    "            \"exists(<integer>)\" : 1,\n",
    "            \"exists(<integer> == <digit><integer>)\" : 1,\n",
    "            \"exists(<integer> == <digit>)\" : 1,\n",
    "            \"exists(<digit>)\" : 1,\n",
    "            \"exists(<digit> == 0)\" : 1,\n",
    "            \"exists(<digit> == 1)\" : 0,\n",
    "            \"exists(<digit> == 2)\" : 0,\n",
    "            \"exists(<digit> == 3)\" : 0,\n",
    "            \"exists(<digit> == 4)\" : 0,\n",
    "            \"exists(<digit> == 5)\" : 0,\n",
    "            \"exists(<digit> == 6)\" : 0,\n",
    "            \"exists(<digit> == 7)\" : 0,\n",
    "            \"exists(<digit> == 8)\" : 0,\n",
    "            \"exists(<digit> == 9)\" : 1,\n",
    "            \"num(<term>)\" : -900.0,\n",
    "            \"num(<value>)\" : 900.0,\n",
    "            \"num(<digit>)\" : 9.0,\n",
    "            \"num(<integer>)\" : 900.0\n",
    "        }, \n",
    "        \"sin(24)\": {\n",
    "            \"exists(<start>)\" : 1,\n",
    "            \"exists(<start> == <function>(<term>))\" : 1,\n",
    "            \"exists(<function>)\" : 1,\n",
    "            \"exists(<function> == sqrt)\" : 0,\n",
    "            \"exists(<function> == tan)\" : 0,\n",
    "            \"exists(<function> == cos)\" : 0,\n",
    "            \"exists(<function> == sin)\" : 1,\n",
    "            \"exists(<term>)\" : 1,\n",
    "            \"exists(<term> == -<value>)\" : 0,\n",
    "            \"exists(<term> == <value>)\" : 1,\n",
    "            \"exists(<value>)\" : 1,\n",
    "            \"exists(<value> == <integer>.<integer>)\" : 0,\n",
    "            \"exists(<value> == <integer>)\" : 1,\n",
    "            \"exists(<integer>)\" : 1,\n",
    "            \"exists(<integer> == <digit><integer>)\" : 1,\n",
    "            \"exists(<integer> == <digit>)\" : 1,\n",
    "            \"exists(<digit>)\" : 1,\n",
    "            \"exists(<digit> == 0)\" : 0,\n",
    "            \"exists(<digit> == 1)\" : 0,\n",
    "            \"exists(<digit> == 2)\" : 1,\n",
    "            \"exists(<digit> == 3)\" : 0,\n",
    "            \"exists(<digit> == 4)\" : 1,\n",
    "            \"exists(<digit> == 5)\" : 0,\n",
    "            \"exists(<digit> == 6)\" : 0,\n",
    "            \"exists(<digit> == 7)\" : 0,\n",
    "            \"exists(<digit> == 8)\" : 0,\n",
    "            \"exists(<digit> == 9)\" : 0,\n",
    "            \"num(<term>)\" : 24.0,\n",
    "            \"num(<value>)\" : 24.0,\n",
    "            \"num(<digit>)\" : 4.0,\n",
    "            \"num(<integer>)\" : 24.0\n",
    "        },\n",
    "        \"cos(-3.14)\": {\n",
    "            \"exists(<start>)\" : 1,\n",
    "            \"exists(<start> == <function>(<term>))\" : 1,\n",
    "            \"exists(<function>)\" : 1,\n",
    "            \"exists(<function> == sqrt)\" : 0,\n",
    "            \"exists(<function> == tan)\" : 0,\n",
    "            \"exists(<function> == cos)\" : 1,\n",
    "            \"exists(<function> == sin)\" : 0,\n",
    "            \"exists(<term>)\" : 1,\n",
    "            \"exists(<term> == -<value>)\" : 1,\n",
    "            \"exists(<term> == <value>)\" : 0,\n",
    "            \"exists(<value>)\" : 1,\n",
    "            \"exists(<value> == <integer>.<integer>)\" : 1,\n",
    "            \"exists(<value> == <integer>)\" : 0,\n",
    "            \"exists(<integer>)\" : 1,\n",
    "            \"exists(<integer> == <digit><integer>)\" : 1,\n",
    "            \"exists(<integer> == <digit>)\" : 1,\n",
    "            \"exists(<digit>)\" : 1,\n",
    "            \"exists(<digit> == 0)\" : 0,\n",
    "            \"exists(<digit> == 1)\" : 1,\n",
    "            \"exists(<digit> == 2)\" : 0,\n",
    "            \"exists(<digit> == 3)\" : 1,\n",
    "            \"exists(<digit> == 4)\" : 1,\n",
    "            \"exists(<digit> == 5)\" : 0,\n",
    "            \"exists(<digit> == 6)\" : 0,\n",
    "            \"exists(<digit> == 7)\" : 0,\n",
    "            \"exists(<digit> == 8)\" : 0,\n",
    "            \"exists(<digit> == 9)\" : 0,\n",
    "            \"num(<term>)\" : -3.14,\n",
    "            \"num(<value>)\" : 3.14,\n",
    "            \"num(<digit>)\" : 4.0,\n",
    "            \"num(<integer>)\" : 14.0\n",
    "        }\n",
    "    }\n",
    "\n",
    "    all_features = get_all_features(CALC_GRAMMAR)\n",
    "    for sample in sample_list:\n",
    "        input_features = compute_feature_values(sample, CALC_GRAMMAR, all_features)\n",
    "\n",
    "        for feature in all_features:\n",
    "            key = feature.name_rep()\n",
    "            #print(f\"\\t{key.ljust(50)}: {input_features[key]}\")\n",
    "            #print('\"' + key + '\"' + ' : ' + str(input_features[key]) + ',')\n",
    "            expected = expected_feature_values[sample][key]\n",
    "            actual = input_features[key]\n",
    "            assert (expected == actual), f\"Wrong feature value for sample {sample} and feature {key}: expected {expected} but is {actual}.\"\n",
    "            \n",
    "    print(\"All checks passed!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "id": "1f79122d",
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'key' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Input \u001b[1;32mIn [44]\u001b[0m, in \u001b[0;36m<cell line: 2>\u001b[1;34m()\u001b[0m\n\u001b[0;32m      1\u001b[0m \u001b[38;5;66;03m# Uncomment to execute test\u001b[39;00m\n\u001b[1;32m----> 2\u001b[0m \u001b[43mtest_feature_values\u001b[49m\u001b[43m(\u001b[49m\u001b[43m)\u001b[49m\n",
      "Input \u001b[1;32mIn [43]\u001b[0m, in \u001b[0;36mtest_feature_values\u001b[1;34m()\u001b[0m\n\u001b[0;32m      3\u001b[0m sample_list \u001b[38;5;241m=\u001b[39m [\u001b[38;5;124m\"\u001b[39m\u001b[38;5;124msqrt(-900)\u001b[39m\u001b[38;5;124m\"\u001b[39m, \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124msin(24)\u001b[39m\u001b[38;5;124m\"\u001b[39m, \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124mcos(-3.14)\u001b[39m\u001b[38;5;124m\"\u001b[39m]\n\u001b[0;32m      5\u001b[0m expected_feature_values \u001b[38;5;241m=\u001b[39m {\n\u001b[0;32m      6\u001b[0m     \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124msqrt(-900)\u001b[39m\u001b[38;5;124m\"\u001b[39m: {\n\u001b[0;32m      7\u001b[0m         \u001b[38;5;124m\"\u001b[39m\u001b[38;5;124mexists(<start>)\u001b[39m\u001b[38;5;124m\"\u001b[39m : \u001b[38;5;241m1\u001b[39m,\n\u001b[1;32m   (...)\u001b[0m\n\u001b[0;32m    104\u001b[0m     }\n\u001b[0;32m    105\u001b[0m }\n\u001b[1;32m--> 107\u001b[0m all_features \u001b[38;5;241m=\u001b[39m \u001b[43mget_all_features\u001b[49m\u001b[43m(\u001b[49m\u001b[43mCALC_GRAMMAR\u001b[49m\u001b[43m)\u001b[49m\n\u001b[0;32m    108\u001b[0m \u001b[38;5;28;01mfor\u001b[39;00m sample \u001b[38;5;129;01min\u001b[39;00m sample_list:\n\u001b[0;32m    109\u001b[0m     input_features \u001b[38;5;241m=\u001b[39m compute_feature_values(sample, CALC_GRAMMAR, all_features)\n",
      "Input \u001b[1;32mIn [35]\u001b[0m, in \u001b[0;36mget_all_features\u001b[1;34m(grammar)\u001b[0m\n\u001b[0;32m      1\u001b[0m \u001b[38;5;28;01mdef\u001b[39;00m \u001b[38;5;21mget_all_features\u001b[39m(grammar: Grammar) \u001b[38;5;241m-\u001b[39m\u001b[38;5;241m>\u001b[39m List[Feature]:\n\u001b[1;32m----> 2\u001b[0m     \u001b[38;5;28;01mreturn\u001b[39;00m \u001b[43mextract_existence\u001b[49m\u001b[43m(\u001b[49m\u001b[43mgrammar\u001b[49m\u001b[43m)\u001b[49m \u001b[38;5;241m+\u001b[39m extract_numeric(grammar)\n",
      "Input \u001b[1;32mIn [33]\u001b[0m, in \u001b[0;36mextract_existence\u001b[1;34m(grammar)\u001b[0m\n\u001b[0;32m      8\u001b[0m list_existence \u001b[38;5;241m=\u001b[39m []\n\u001b[0;32m      9\u001b[0m \u001b[38;5;28;01mfor\u001b[39;00m rule \u001b[38;5;129;01min\u001b[39;00m grammar:\n\u001b[1;32m---> 10\u001b[0m     \u001b[38;5;28;01mif\u001b[39;00m rule \u001b[38;5;241m==\u001b[39m \u001b[43mkey\u001b[49m:\n\u001b[0;32m     11\u001b[0m         list_existence\u001b[38;5;241m.\u001b[39minsert([\u001b[38;5;124m'\u001b[39m\u001b[38;5;124mexists(\u001b[39m\u001b[38;5;132;01m{self.rule}\u001b[39;00m\u001b[38;5;124m)\u001b[39m\u001b[38;5;124m'\u001b[39m])\n\u001b[0;32m     12\u001b[0m     \u001b[38;5;28;01melse\u001b[39;00m:\n",
      "\u001b[1;31mNameError\u001b[0m: name 'key' is not defined"
     ]
    }
   ],
   "source": [
    "# Uncomment to execute test\n",
    "test_feature_values()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d056d1a",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
